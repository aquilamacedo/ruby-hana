#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: hana 1.3.7 ruby lib

Gem::Specification.new do |s|
  s.name = "hana".freeze
  s.version = "1.3.7"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "homepage_uri" => "http://github.com/tenderlove/hana" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Aaron Patterson".freeze]
  s.date = "2020-12-03"
  s.description = "Implementation of [JSON Patch][1] and [JSON Pointer][2] RFC.".freeze
  s.email = ["aaron@tenderlovemaking.com".freeze]
  s.extra_rdoc_files = ["Manifest.txt".freeze, "README.md".freeze]
  s.files = ["Manifest.txt".freeze, "README.md".freeze, "Rakefile".freeze, "lib/hana.rb".freeze, "test/helper.rb".freeze, "test/json-patch-tests/.editorconfig".freeze, "test/json-patch-tests/.npmignore".freeze, "test/json-patch-tests/README.md".freeze, "test/json-patch-tests/package.json".freeze, "test/json-patch-tests/spec_tests.json".freeze, "test/json-patch-tests/tests.json".freeze, "test/mine.json".freeze, "test/test_hana.rb".freeze, "test/test_ietf.rb".freeze]
  s.homepage = "http://github.com/tenderlove/hana".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--main".freeze, "README.md".freeze]
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "Implementation of [JSON Patch][1] and [JSON Pointer][2] RFC.".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<hoe>.freeze, ["~> 3.22"])
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5.14"])
      s.add_development_dependency(%q<rdoc>.freeze, ["< 7", ">= 4.0"])
    else
      s.add_dependency(%q<hoe>.freeze, ["~> 3.22"])
      s.add_dependency(%q<minitest>.freeze, ["~> 5.14"])
      s.add_dependency(%q<rdoc>.freeze, ["< 7", ">= 4.0"])
    end
  else
    s.add_dependency(%q<hoe>.freeze, ["~> 3.22"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.14"])
    s.add_dependency(%q<rdoc>.freeze, ["< 7", ">= 4.0"])
  end
end
